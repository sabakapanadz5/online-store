import { categories, products } from "./API.js";
import { innerHtml } from "./products.js";
const productHtml = (allProducts) => {
  let innerHTML = " ";
  for (const product of allProducts) {
    innerHTML += innerHtml(product);
  }
  return innerHTML;
};
// catalog
const catalog = document.getElementsByClassName("catalog")[0];

const assemblyCatalog = async (sort) => {
  const allProducts = await products(sort);
  catalog.innerHTML = productHtml(allProducts);
};
assemblyCatalog();

// search
const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");

searchButton.addEventListener("click", () => {
  products().then((result) => {
    const filerProducts = result.filter((product) =>
      product.title.toLowerCase().includes(searchQuery.value.toLowerCase())
    );
    catalog.innerHTML = productHtml(filerProducts);
  });
});
// sort
const sort = document.getElementById("sort");
sort.addEventListener("change", () => {
  assemblyCatalog(sort.value);
});
// total products
const totalProducts = document.getElementById("totalProducts");
products().then((result) => {
  let innerHTML = "";
  innerHTML = `selected 0 out of ${result.length} products`;
  totalProducts.innerHTML = innerHTML;
});
