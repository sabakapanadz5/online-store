import { ADRESS } from "./adress.js";

const call = async (url) => {
  const request = await fetch(ADRESS + url);
  const result = await request.json();
  return result;
};
// products
export const products = async (sort) =>
  await call(`products${sort ? `?sort=${sort}` : ""}`);
// categories
export const categories = async () => {
  const result = await call("products/categories");
  return result;
};
